package org.study;

import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;

/**
 * Created by Kuzmenko on 24.01.2017.
 */
public class Main {
    /**
     * Main method.
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = Server.startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", Server.BASE_URI));
        System.in.read();
        server.shutdown();
    }
}
