package org.study;

import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import javax.ws.rs.*;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by Kuzmenko on 24.01.2017.
 */
@Path("/service")
public class ListProcessorService {

    @GET
    @Path("/echo")
    public Response echo() {
        return Response.status(201).build();
    }

    @POST
    @Path("/listAnalysis")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listAnalysis(List<Integer> list) {
        List<String> response = new ArrayList<>(list.size());
        Set<Integer> uniqueSet = new HashSet<>(list);
        for (Integer temp : uniqueSet) {
            response.add(temp + ": " + Collections.frequency(list, temp) + " times");
        }
        return Response.status(201).entity(Entity.json(response)).build();
    }

    @POST
    @Path("/filterList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response filterList(List<Integer>[] lists) {
        List<Integer> sourceList = lists[0];
        List<Integer> checkList = lists[1];
        List<Integer> response;
        Set<Integer> uniqueSet = new HashSet<>(checkList);
        Collection<Integer> filtered = Collections2.filter(sourceList, Predicates.in(uniqueSet));
        response = Lists.newArrayList(filtered);
        return Response.status(201).entity(Entity.json(response)).build();
    }
}

