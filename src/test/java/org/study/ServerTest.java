package org.study;

import org.glassfish.grizzly.http.server.HttpServer;
import org.json.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Unit test for simple Server.
 */
public class ServerTest {
    private HttpServer server;
    private WebTarget target;
    private final Random random = new Random();

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Server.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
        // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(Server.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    @Test
    public void echoTest() {
        System.out.println("Call service /echo");
        Response response = target.path("/service/echo").request().get();
        System.out.println("HttpResponse: " + response.getStatus());
        System.out.println("Server " + ((response.getStatus()==201) ? "OK" : "Not OK"));
    }

    @Test
    public void testListAnalysis() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(random.nextInt(10) + 1);
        }
        System.out.println("Call service /service/listAnalysis with parameter: list = " + list.toString());
        Response response = target.path("/service/listAnalysis").request().post(Entity.json(list));
        System.out.println("HttpResponse: " + response.getStatus());
        JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
        JSONArray array = jsonObject.getJSONArray("entity");
        System.out.println("Count of duplicates in list: ");
        for (int i = 0; i < array.length(); i++) {
            System.out.println(array.get(i));
        }
    }

    @Test
    public void testFilterList() {
        List<Integer> sourceList = new ArrayList<>();
        List<Integer> checkList = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            sourceList.add(random.nextInt(10) + 1);
        }
        for (int i = 0; i < 5; i++) {
            checkList.add(random.nextInt(10) + 1);
        }

        System.out.println("Call service /service/filterList with parameter:");
        System.out.println("sourceList: " + sourceList.toString());
        System.out.println("checkList: " + checkList.toString());
        List<Integer> [] lists = new  List[2];
        lists[0]=sourceList;
        lists[1]=checkList;
        Response response = target.path("/service/filterList").request().post(Entity.json(lists));
        System.out.println("HttpResponse: " + response.getStatus());
        JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
        JSONArray array = jsonObject.getJSONArray("entity");
        System.out.println("Filtered list: ");
        for (int i = 0; i < array.length(); i++) {
            result.add((Integer) array.get(i));
        }
        System.out.println(result);
    }
}
